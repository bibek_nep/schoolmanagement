from django.db import models
from sqlalchemy import *
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import *
from sqlalchemy import types


Base = declarative_base()	

class Test(Base):
	__tablename__ = 'test'
	id = Column(Integer,primary_key=True)
	name = Column(String)